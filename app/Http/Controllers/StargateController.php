<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

class StargateController extends Controller
{
    public function __invoke()
    {
        // Helper function
        function value_or_default($key, array $array, $default = null)
        {
            return array_key_exists($key, $array) ? $array[$key] : $default;
        }

        // Instantiate an instance of the GuzzleHttp Client
        $client = new Client();

        // Get request attributes
        $headers = getallheaders();
        $method = value_or_default('REQUEST_METHOD', $_SERVER);
        $url = value_or_default('X-Proxy-Url', $headers);

        // Check that we have a URL
        if( ! $url)
            http_response_code(400) and exit("X-Proxy-Url header missing");

        // Check that the URL looks like an absolute URL
        if( ! parse_url($url, PHP_URL_SCHEME))
            http_response_code(403) and exit("Not an absolute URL: $url");

        // Remove ignored headers and prepare the rest for resending
        $ignore = ['Cookie', 'Host', 'X-Proxy-URL'];
        $headers = array_diff_key($headers, array_flip($ignore));
        $body = "";

        // Method specific options
        switch($method)
        {
            case 'GET':
                break;
            case 'PUT':
            case 'POST':
            case 'DELETE':
            default:
                // Capture the post body of the request to send along
                $body = file_get_contents('php://input');
                break;
        }

        try {
            //Create an HTTP request
            $response = $client->request($method, $url, ['headers' => $headers,'body' => $body, 'decode_content' => false]);
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $response=$e->getResponse();
            }
        } catch (ServerException $e) {
            echo $e;
            echo "=================";
            if ($e->hasResponse()) {
                $response=$e->getResponse();
            }
        }

        // Remove any existing headers
        header_remove();

        //Print the response code
        http_response_code($response->getStatusCode());

        return response($response->getBody())
            ->withHeaders($response->getHeaders());
    }
}
