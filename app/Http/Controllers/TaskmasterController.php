<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TaskmasterController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(array(
            "headers" => $request->header(),
            "cookies" => $request->cookies->all(),
            "serverVariables" => $request->server(),
            "ip" => $request->ip(),
            "ajax" => $request->ajax(),
            "pjax" => $request->pjax(),
            "content" => $request->all(),
            "body" => $request->getContent()
        ));
    }
}
