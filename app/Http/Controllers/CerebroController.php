<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

// Geolocation controller
use GeoIp2\Database\Reader;

class CerebroController extends Controller
{
    /**
     * Geolocate user either by Cloudflare or GeoLocation DB
     *
     * @return JSON
     */
    public function __invoke(Request $request)
    {
        $response = [];

        if ($request->hasHeader('CF-IPCountry')) {
            $response['country'] = $request->header('CF-IPCountry');
        } else {
            $reader = new Reader(base_path('resources/geolocation/GeoLite2-City.mmdb'));
            try {
                if ($request->input('ip') != NULL) {
                    $ip = $request->input('ip');
                } else {
                    $ip = $request->ip();
                }
                $record = $reader->city($ip);
            } catch (\Exception $e) {
                abort(404);
            }

            $response['country'] = $record->country->isoCode;
            $response['countryFullName'] = $record->country->name;

            $response['subdivision'] = $record->mostSpecificSubdivision->isoCode;
            $response['subdivisionFullName'] = $record->mostSpecificSubdivision->name;

            $response['city'] = $record->city->name;
            $response['postal'] = $record->postal->code;
        }

        return response()->json($response);
    }
}