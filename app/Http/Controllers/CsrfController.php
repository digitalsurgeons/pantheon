<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class CsrfController extends Controller
{
    public function __invoke()
    {
        return csrf_token();
    }
}
