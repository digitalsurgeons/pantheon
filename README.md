![](https://gitlab.com/digitalsurgeons/pantheon/raw/master/assets/logo.svg)

## A collection of useful APIs

### Introduction

Pantheon is a Laravel application that is a collection of useful APIs that can be used across our production and development work.

Pantheon can be accessed at:

- https://pantheon.digitalsurgeonsdev.com (development)
- https://pantheon.digitalsurgeons.cloud (production)

### Requirements

PHP >= 7.2

Apache

### Working Locally

1. `composer install`
2. Copy the `.env.example` to `.env`
3. `php artisan key:generate`
4. fin.

### The APIs

#### Cerebro

Cerebro is a geolocation API that uses [Maxmind's GeoLite2 City Database](https://dev.maxmind.com/geoip/geoip2/geolite2/)

Simply sending a `get` requestion to `/cerebro` will return a JSON object containing location data. For example:

```
GET /cerebro HTTP/1.1
Host: pantheon.digitalsurgeonsdev.com
Connection: close
```

```
HTTP/1.1 200 OK
Content-Length: 139
Connection: close
Content-Type: application/json
```

```json
{
  "country": "US",
  "countryFullName": "United States",
  "subdivision": "CT",
  "subdivisionFullName": "Connecticut",
  "city": "Milford",
  "postal": "06460"
}
```



Additionally, you can manually specify an IP through an `ip` query parameter or `ip` form parameter as a `POST` request:

```
GET /cerebro?ip=69.162.81.155 HTTP/1.1
Host: pantheon.digitalsurgeonsdev.com
Connection: close
```

```
HTTP/1.1 200 OK
Content-Length: 132
Connection: close
Content-Type: application/json
```

```json
{
  "country": "US",
  "countryFullName": "United States",
  "subdivision": "TX",
  "subdivisionFullName": "Texas",
  "city": "Dallas",
  "postal": "75202"
}
```



Cerebro also works with IPV6, and additionally will work behind cloudflare if a `CF-IPCountry` is present.

#### Taskmaster

`/taskmaster` returns information about your request, as well as the content of your request itself, back to you. It's useful for troubleshooting connectivity and outgoing requests.

For example with a GET request:

```
GET /taskmaster?queryparameter=true HTTP/1.1
Host: pantheon.digitalsurgeonsdev.com
Connection: close
```

```
HTTP/1.1 200 OK
Content-Length: 1543
Connection: close
Content-Type: application/json
```

```json
{
  "headers": {
    "host": [
      "pantheon.digitalsurgeonsdev.com"
    ],
    "connection": [
      "close"
    ],
    "user-agent": [
      "Paw\/3.1.9 (Macintosh; OS X\/10.15.0) GCDHTTPRequest"
    ]
  },
  "cookies": [],
  "serverVariables": {
    "REDIRECT_UNIQUE_ID": "XbC3-38AAQEAAE83H04AAAAD",
    "REDIRECT_STATUS": "200",
    "UNIQUE_ID": "XbC3-38AAQEAAE83H04AAAAD",
    "HTTP_HOST": "pantheon.digitalsurgeonsdev.com",
    "HTTP_CONNECTION": "close",
    "HTTP_USER_AGENT": "Paw\/3.1.9 (Macintosh; OS X\/10.15.0) GCDHTTPRequest",
    "PATH": "\/usr\/local\/sbin:\/usr\/local\/bin:\/usr\/sbin:\/usr\/bin:\/sbin:\/bin",
    "SERVER_SIGNATURE": "<address>Apache\/2.4.18 (Ubuntu) Server at pantheon.digitalsurgeonsdev.com Port 80<\/address>\n",
    "SERVER_SOFTWARE": "Apache\/2.4.18 (Ubuntu)",
    "SERVER_NAME": "pantheon.digitalsurgeonsdev.com",
    "SERVER_ADDR": "162.243.173.73",
    "SERVER_PORT": "80",
    "REMOTE_ADDR": "[redacted]",
    "DOCUMENT_ROOT": "\/var\/www\/pantheon\/public_html",
    "REQUEST_SCHEME": "http",
    "CONTEXT_PREFIX": "",
    "CONTEXT_DOCUMENT_ROOT": "\/var\/www\/pantheon\/public_html",
    "SERVER_ADMIN": "[no address given]",
    "SCRIPT_FILENAME": "\/var\/www\/pantheon\/public_html\/index.php",
    "REMOTE_PORT": "63698",
    "REDIRECT_URL": "\/taskmaster",
    "REDIRECT_QUERY_STRING": "queryparameter=true",
    "GATEWAY_INTERFACE": "CGI\/1.1",
    "SERVER_PROTOCOL": "HTTP\/1.1",
    "REQUEST_METHOD": "GET",
    "QUERY_STRING": "queryparameter=true",
    "REQUEST_URI": "\/taskmaster?queryparameter=true",
    "SCRIPT_NAME": "\/index.php",
    "PHP_SELF": "\/index.php",
    "REQUEST_TIME_FLOAT": 1571862527.11,
    "REQUEST_TIME": 1571862527
  },
  "ip": "[redacted]",
  "ajax": false,
  "pjax": false,
  "content": {
    "queryparameter": "true"
  },
  "body": ""
}
```



#### Stargate

`/stargate` is a super simple proxy. Simply send the request you want to send to another server, include a `X-Proxy-Url` of the fully qualified domain name, and voila. Cookies, headers, parameters, and POST content will all be included.

```
GET /stargate?queryparameter=true HTTP/1.1
X-Proxy-Url: https://pantheon.digitalsurgeonsdev.com/cerebro
Host: pantheon.digitalsurgeonsdev.com
Connection: close
```

```
HTTP/1.1 200 OK
Content-Length: 143
Connection: close
Content-Type: application/json
```

```json
{
  "country": "US",
  "countryFullName": "United States",
  "subdivision": "NJ",
  "subdivisionFullName": "New Jersey",
  "city": "North Bergen",
  "postal": "07047"
}
```

